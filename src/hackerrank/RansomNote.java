package hackerrank;

import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Scanner;

public class RansomNote {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int m = in.nextInt();
		int n = in.nextInt();
		String magazine[] = new String[m];
		for (int magazine_i = 0; magazine_i < m; magazine_i++) {
			magazine[magazine_i] = in.next();
		}
		String ransom[] = new String[n];
		for (int ransom_i = 0; ransom_i < n; ransom_i++) {
			ransom[ransom_i] = in.next();
		}
		in.close();
		Hashtable<String, Integer> magazineCount = getCountByWord(magazine);
		Hashtable<String, Integer> noteCount = getCountByWord(ransom);
		boolean isEnough = true;
		for (Entry<String, Integer> countFromNote : noteCount.entrySet()) {
			String word = countFromNote.getKey();
			Integer count = countFromNote.getValue();
			Integer countFromMagazine = magazineCount.get(word);
			if (countFromMagazine == null || countFromMagazine < count) {
				isEnough = false;
			}
		}
		String result = isEnough ? "Yes" : "No";
		System.out.println(result);
	}

	private static Hashtable<String, Integer> getCountByWord(String[] array) {
		Hashtable<String, Integer> countByWord = new Hashtable<>();
		for (String word : array) {
			Integer wordCount = countByWord.get(word);
			if (wordCount == null) {
				countByWord.put(word, 1);
			} else {
				countByWord.put(word, wordCount + 1);
			}
		}
		return countByWord;
	}

}
