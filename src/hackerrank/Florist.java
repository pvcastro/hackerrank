package hackerrank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Florist {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String numberSize = scan.nextLine();
        StringTokenizer numberSizeTokenizer = new StringTokenizer(numberSize);
        int numberOfFlowers = Integer.valueOf(numberSizeTokenizer.nextToken());
        int groupSize = Integer.valueOf(numberSizeTokenizer.nextToken());
        String pricesLine = scan.nextLine();
        StringTokenizer pricesTokenizer = new StringTokenizer(pricesLine);
        List<Integer> prices = new ArrayList<>();
        while (pricesTokenizer.hasMoreElements()) {
            prices.add(Integer.valueOf(pricesTokenizer.nextToken()));
        }
        scan.close();
        // Most expensive flowers first
        prices.sort(Comparator.reverseOrder());
        // Start weight of flowers
        int weight = 0;
        // Sum of prices
        int sum = 0;
        // Split list in groups of groupSize
        for (int index = 0; index < prices.size(); index++) {
            // remainder of index by group size is weight of the round
            int roundWeight = index / groupSize;
            weight = roundWeight + 1;
            int price = prices.get(index);
            int actualPrice = weight * price;
            sum = sum + actualPrice;
        }
        System.out.println(sum);
    }
    
    // 0, 1, 2, 3, 4, 5, 6
    // 3 flowers per person
    // 0, 1 -> normal price
    // 2, 3 -> doubled
    // 4, 5 -> tripled
    // 6 -> quadrupled
}
