package hackerrank;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CuttingBoards {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberOfQueries = Integer.parseInt(scan.nextLine());
        int remainingQueries = numberOfQueries;
        BigInteger denominator = BigInteger.valueOf((long) Math.pow(10, 9) + 7);
        while (remainingQueries > 0) {
            String descriptor = scan.nextLine();
            StringTokenizer descriptorTokenizer = new StringTokenizer(descriptor);
            int height = Integer.parseInt(descriptorTokenizer.nextToken());
            int width = Integer.parseInt(descriptorTokenizer.nextToken());
            int horizontalCuts = height - 1;
            int verticalCuts = width - 1;
            List<Cut> cuts = new ArrayList<>();
            addCuts(scan.nextLine(), horizontalCuts, true, cuts);
            addCuts(scan.nextLine(), verticalCuts, false, cuts);
            Comparator<Cut> comparator = (cut1, cut2) -> cut1.getCost().compareTo(cut2.getCost());
            cuts.sort(comparator.reversed());
            //
            BigInteger totalCost = BigInteger.ZERO;
            BigInteger horizontalCutsSoFar = BigInteger.ZERO;
            BigInteger verticalCutsSoFar = BigInteger.ZERO;
            //
            for (Cut cut : cuts) {
                totalCost = totalCost.add(getCost(cut, horizontalCutsSoFar, verticalCutsSoFar));
                if (cut.isHorizontal()) {
                    horizontalCutsSoFar = horizontalCutsSoFar.add(BigInteger.ONE);
                } else {
                    verticalCutsSoFar = verticalCutsSoFar.add(BigInteger.ONE);
                }
            }
            //
            System.out.println((totalCost.remainder(denominator)));
            remainingQueries--;
        }
        scan.close();
    }

    private static BigInteger getCost(Cut cut, BigInteger horizontalCutsSoFar, BigInteger verticalCutsSoFar) {
        if (cut.isHorizontal()) {
            return cut.getCost().multiply((verticalCutsSoFar.add(BigInteger.ONE)));
        } else {
            return cut.getCost().multiply((horizontalCutsSoFar.add(BigInteger.ONE)));
        }
    }

    private static void addCuts(String line, int size, boolean horizontal, List<Cut> list) {
        String[] stringArray = line.split(" ");
        for (int i = 0; i < stringArray.length; i++) {
            list.add(new Cut(horizontal, stringArray[i]));
        }
    }

    private static class Cut {

        private boolean    horizontal;
        private BigInteger cost;

        public Cut(boolean horizontal, String cost) {
            super();
            this.horizontal = horizontal;
            this.cost = new BigInteger(cost);
        }

        public boolean isHorizontal() {
            return horizontal;
        }

        public BigInteger getCost() {
            return cost;
        }

    }

}
