package hackerrank;

import java.util.Scanner;

public class LetsReview {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int i = 0; i < T; i++) {
			String string = sc.next();
			String even = "";
			String odd = "";
			for (int j = 0; j < string.length(); j++) {
				if(j % 2 == 0) {
					even += string.toCharArray()[j];
				} else {
					odd += string.toCharArray()[j];
				}
			}
			System.out.println(even + " " + odd);
		}
		sc.close();
    }
	
}
