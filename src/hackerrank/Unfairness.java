package hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Unfairness {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(in.readLine());
        int K = Integer.parseInt(in.readLine());
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            list.add(Integer.parseInt(in.readLine()));
        }

        list.sort(Comparator.naturalOrder());

        /*
         * Write your code here, to process numPackets N, numKids K, and the packets of candies Compute the ideal value for unfairness over here
         */

        int i = 0;
        int j = K - 1;
        int unfairness = Integer.MAX_VALUE;

        while (j <= list.size() - 1) {
            List<Integer> subset = list.subList(i, j + 1);
            subset.sort(Comparator.naturalOrder());
            int max = subset.get(K - 1);
            int min = subset.get(0);
            if ((max - min) < unfairness) {
                unfairness = max - min;
            }
            i++;
            j++;
        }

        System.out.println(unfairness);
    }

}
