package hackerrank;

import java.util.Scanner;

public class BinaryNumbers {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		sc.close();
        String binary = Integer.toBinaryString(N);
        int count = 0;
        int largestCount = 0;
        for (Character character : binary.toCharArray()) {
            if (character == '0') {
                count = 0;
            } else if (character == '1') {
                count++;
            }
            if (count > largestCount) {
                largestCount = count;
            }
        }
        System.out.println(largestCount);
    }

}
