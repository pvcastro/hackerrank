package hackerrank;

import java.util.Scanner;

class Difference {
	
	private int[] elements;
	
	public int maximumDifference;
	
	public Difference(int[] elements) {
		this.elements = elements;
	}

	public void computeDifference() {
		maximumDifference = 0;
		int i = 0;
		while (i < elements.length) {
			for (int j = 0; j < elements.length; j++) {
				if (i != j) {
					int sum = Math.abs(elements[i] - elements[j]);
					if (sum > maximumDifference) {
						maximumDifference = sum;
					}
				}
			}
			i++;
		}
	}

}

public class Scope {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		sc.close();
		Difference difference = new Difference(a);
		difference.computeDifference();
		System.out.print(difference.maximumDifference);
	}

}
