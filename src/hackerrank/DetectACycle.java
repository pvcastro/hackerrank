package hackerrank;

import java.util.HashSet;
import java.util.Set;

class Node {
	int data;
	Node next;
}

public class DetectACycle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	boolean hasCycle(Node head) {
//		if (head == null || head.next == null) {
//			return false;
//		}
//		Set<Integer> dataVisited = new HashSet<>();
//		Node tmp = head;
//		while(tmp != null) {
//			if (!dataVisited.contains(tmp.data)) {
//				dataVisited.add(tmp.data);
//				tmp = head.next;
//			} else {
//				return true;
//			}
//		}
//		return false;
		Set<Node> seen = new HashSet<>();
	    while (head != null) {
	        seen.add(head);
	        head = head.next;
	        if (seen.contains(head)) {
	        	return true;
	        }
	    }
	    return false;
	}

}
