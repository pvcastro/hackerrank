package hackerrank;

import java.util.Scanner;

public class Recursion {

	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        sc.close();
        System.out.println(factorial(N));
    }
	
	public static int factorial(int N) {
		if (N <= 1) {
			return 1;
		} else {
			return N * factorial(N - 1);
		}
	}
	
}
