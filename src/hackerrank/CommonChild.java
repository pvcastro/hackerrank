package hackerrank;

import java.util.Scanner;

public class CommonChild {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		char[] sArray = s.toCharArray();
		String r = sc.next();
		char[] rArray = r.toCharArray();
		sc.close();
		int m = s.length();
		int n = r.length();
		int[][] matrix = new int[5001][5001];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (sArray[i] == rArray[j]) {
					matrix[i + 1][j + 1] = matrix[i][j] + 1;
				} else {
					matrix[i + 1][j + 1] = Math.max(matrix[i + 1][j], matrix[i][j + 1]);
				}
			}
		}
		System.out.println(matrix[m][n]);
	}

}
