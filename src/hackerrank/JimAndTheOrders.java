package hackerrank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class JimAndTheOrders {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number = Integer.parseInt(scan.nextLine());
        // Orders
        List<Order> orders = new ArrayList<>();
        int orderNumber = 1;
        while (scan.hasNextLine()) {
            String order = scan.nextLine();
            StringTokenizer orderTokenizer = new StringTokenizer(order);
            int orderTime = Integer.parseInt(orderTokenizer.nextToken());
            int orderDuration = Integer.parseInt(orderTokenizer.nextToken());
            int fulfillmentTime = orderTime + orderDuration;
            orders.add(new Order(orderNumber, fulfillmentTime));
            orderNumber++;
        }
        scan.close();
        //
        orders.sort(new OrderComparator());
        //
        String output = "";
        for (Order order : orders) {
            output += order.getOrderNumber().toString() + " ";
        }
        System.out.println(output.trim());
    }

    private static class Order {
        private Integer orderNumber;
        private Integer orderFulfillmentTime;

        public Order(Integer orderNumber, Integer orderFulfillmentTime) {
            this.orderNumber = orderNumber;
            this.orderFulfillmentTime = orderFulfillmentTime;
        }

        public Integer getOrderNumber() {
            return orderNumber;
        }

        public Integer getOrderFulfillmentTime() {
            return orderFulfillmentTime;
        }
    }

    private static class OrderComparator implements Comparator<Order> {

        @Override
        public int compare(Order order1, Order order2) {
            if (order1.getOrderFulfillmentTime().equals(order2.getOrderFulfillmentTime())) {
                return order1.getOrderNumber().compareTo(order2.getOrderNumber());
            }
            return order1.getOrderFulfillmentTime().compareTo(order2.getOrderFulfillmentTime());
        }

    }

}
