package hackerrank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class JourneyToTheMoon {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numberOfAstronauts = sc.nextInt();
		int numberOfLines = sc.nextInt();
		Map<Integer, Set<Integer>> astronautsByCountry = new HashMap<>();
		for (int i = 0; i < numberOfLines; i++) {
			int firstAstronaut = sc.nextInt();
			int secondAstronaut = sc.nextInt();
			Integer country = null;
			Set<Integer> astronauts = null;
			if (containsAstronaut(astronautsByCountry, firstAstronaut)) {
				country = getCountry(astronautsByCountry, firstAstronaut);
			} else if (containsAstronaut(astronautsByCountry, secondAstronaut)) {
				country = getCountry(astronautsByCountry, secondAstronaut);
			}
			if (country == null) {
				country = i;
				astronauts = new HashSet<>();
				astronautsByCountry.put(country, astronauts);
			} else {
				astronauts = astronautsByCountry.get(country);
			}
			astronauts.add(firstAstronaut);
			astronauts.add(secondAstronaut);
		}
		sc.close();
		Integer differentWays = 1;
		for (Set<Integer> astronauts : astronautsByCountry.values()) {
			differentWays *= astronauts.size();
		}
		System.out.println(differentWays);
	}

	private static boolean containsAstronaut(Map<Integer, Set<Integer>> astronautsByCountry, int astronaut) {
		return astronautsByCountry//
				.entrySet()//
				.stream()//
				.anyMatch(entry -> entry.getValue().contains(astronaut));
	}

	private static Integer getCountry(Map<Integer, Set<Integer>> astronautsByCountry, int astronaut) {
		return astronautsByCountry//
				.entrySet()//
				.stream()//
				.filter(entry -> entry.getValue().contains(astronaut))//
				.findFirst()//
				.orElse(null)//
				.getKey();
	}

}
