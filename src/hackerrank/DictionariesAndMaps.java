package hackerrank;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DictionariesAndMaps {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<String,Integer> phoneByName = new HashMap<>();
        for(int i = 0; i < n; i++){
            String name = in.next();
            int phone = in.nextInt();
            // Write code here
            phoneByName.put(name, phone);
        }
        while(in.hasNext()){
            String s = in.next();
            // Write code here
            Integer phone = phoneByName.get(s);
            if (phone == null) {
            	System.out.println("Not found");
            } else {
            	System.out.println(s + "=" + phone);
            }
        }
        in.close();
	}

}
