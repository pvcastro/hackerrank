package hackerrank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Toys {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String quantityMoney = scan.nextLine();
        StringTokenizer quantityMoneyTokenizer = new StringTokenizer(quantityMoney);
        int quantity = Integer.valueOf(quantityMoneyTokenizer.nextToken());
        int money = Integer.valueOf(quantityMoneyTokenizer.nextToken());
        String pricesLine = scan.nextLine();
        StringTokenizer pricesTokenizer = new StringTokenizer(pricesLine);
        List<Integer> prices = new ArrayList<>();
        while (pricesTokenizer.hasMoreElements()) {
            prices.add(Integer.valueOf(pricesTokenizer.nextToken()));
        }
        scan.close();
        prices.sort(Comparator.naturalOrder());
        int remaining = money;
        int output = 0;
        for (int price : prices) {
            if ((remaining - price) > 0) {
                output = output + 1;
                remaining = remaining - price;
            }
        }
        System.out.println(output);
    }

}
