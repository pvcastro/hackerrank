package hackerrank;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Arrays {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int arraySize = Integer.parseInt(in.nextLine());
        String arrayString = in.nextLine();
        StringTokenizer tokenizer = new StringTokenizer(arrayString);
        int[] array = new int[arraySize];
        for (int i = 0; i < array.length; i++) {
			array[i] = Integer.parseInt(tokenizer.nextToken());
		}
        in.close();
        String result = "";
        int currentElement = arraySize - 1;
        for (int i = 0; i < arraySize; i++) {
			result += array[currentElement] + " ";
			currentElement--;
		}
        System.out.println(result.trim());
	}
	
}
