package hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MakingAnagrams {

	public static int numberNeeded(String first, String second) {
		List<Character> firstList = getListOfChars(first);
		List<Character> secondList = getListOfChars(second);
		List<Character> removeFromFirst = new ArrayList<>();
		List<Character> removeFromSecond = new ArrayList<>();
		for (Character character : firstList) {
			if (!secondList.contains(character)) {
				removeFromFirst.add(character);
			} else {
				// Contains, count difference of quantity in each
				long countInFirst = firstList.stream().filter(aChar -> aChar.equals(character)).count();
				long countInSecond = secondList.stream().filter(aChar -> aChar.equals(character)).count();
				if (countInFirst > countInSecond && !removeFromFirst.contains(character)) {
					for (int i = 0; i < countInFirst - countInSecond; i++) {
						removeFromFirst.add(character);
					}
				} else if (countInSecond > countInFirst && !removeFromSecond.contains(character)) {
					for (int i = 0; i < countInSecond - countInFirst; i++) {
						removeFromSecond.add(character);
					}
				}
			}
		}
		removeFromFirst.forEach(aChar -> firstList.remove(aChar));
		for (Character character : secondList) {
			if (!firstList.contains(character)) {
				removeFromSecond.add(character);
			}
		}
		removeFromSecond.forEach(aChar -> secondList.remove(aChar));
		return removeFromFirst.size() + removeFromSecond.size();
	}

	private static List<Character> getListOfChars(String string) {
		return string.chars().mapToObj(aChar -> (char) aChar).collect(Collectors.toList());
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String a = in.next();
		String b = in.next();
		in.close();
		System.out.println(numberNeeded(a, b));
	}

}
