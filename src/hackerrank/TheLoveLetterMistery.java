package hackerrank;

import java.util.Scanner;

public class TheLoveLetterMistery {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int i = 0; i < T; i++) {
			String word = sc.next().trim();
			char[] chars = word.toCharArray();
			int length = word.length();
			int cost = 0;
			int j = 0;
	        int k = length - 1;
	        while (j < Math.round(length / 2)) {
	        	char charAtJ = chars[j];
	        	char charAtK = chars[k];
	        	while (charAtK != charAtJ) {
	        		if (charAtJ > charAtK) {
	        			charAtJ = (char) (charAtJ - 1);
	        		} else if (charAtK > charAtJ) {
	        			charAtK = (char) (charAtK - 1);
	        		}
	        		cost++;
	        	}
				j++;
				k--;
			}
	        System.out.println(cost);
        }
		sc.close();
	}
	
}
