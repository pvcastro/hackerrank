package hackerrank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class PermutingTwoArrays {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberOfQueries = Integer.parseInt(scan.nextLine());
        int remainingQueries = numberOfQueries;
        while (remainingQueries > 0) {
            String descriptor = scan.nextLine();
            StringTokenizer descriptorTokenizer = new StringTokenizer(descriptor);
            int size = Integer.parseInt(descriptorTokenizer.nextToken());
            int relation = Integer.parseInt(descriptorTokenizer.nextToken());
            List<Integer> firstList = getList(scan.nextLine(), size);
            firstList.sort(Comparator.naturalOrder());
            List<Integer> secondList = getList(scan.nextLine(), size);
            secondList.sort(Comparator.reverseOrder());
            boolean no = false;
            for (int i = 0; i < firstList.size(); i++) {
                if ((firstList.get(i) + secondList.get(i)) < relation) {
                    System.out.println("NO");
                    no = true;
                    break;
                }
            }
            if (!no) {
                System.out.println("YES");
            }
            remainingQueries--;
        }
        scan.close();
    }
    
    private static List<Integer> getList(String line, int size) {
        List<Integer> list = new ArrayList<>();
        String[] stringArray = line.split(" ");
        for (int i = 0; i < stringArray.length; i++) {
            list.add(Integer.parseInt(stringArray[i]));
        }
        return list;
    }
    
}
