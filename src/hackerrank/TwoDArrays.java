package hackerrank;

import java.util.Scanner;

public class TwoDArrays {

	public static void main(String[] args) {
		int[][] A = new int[6][6];
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				A[i][j] = sc.nextInt();
			}
		}
		sc.close();
		int largestSum = Integer.MIN_VALUE;
		int sum = Integer.MIN_VALUE;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				int first = A[i][j];
				int second = A[i][j + 1];
				int third = A[i][j + 2];
				int fourth = A[i + 1][j + 1];
				int fifth = A[i + 2][j];
				int sixth = A[i + 2][j + 1];
				int seventh = A[i + 2][j + 2];
				sum = first + second + third + fourth + fifth + sixth + seventh;
				if (sum > largestSum) {
					largestSum = sum;
				}
			}
		}
		System.out.println(largestSum);
	}

}
