package hackerrank;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LeftRotation {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int length = sc.nextInt();
		int K = sc.nextInt();

		String[] result = new String[length];

		int lastIndex = length - 1;

		int actualShift = 0;

		if (length > 0) {
			actualShift = length - (K % length);
		}

		for (int i = 0; i < length; i++) {
			int newIndex;
			if (i + actualShift > lastIndex) {
				newIndex = i + actualShift - length;
			} else {
				newIndex = i + actualShift;
			}
			result[newIndex] = String.valueOf(sc.nextInt());
		}

		sc.close();

		System.out.println(Stream.of(result).collect(Collectors.joining(" ")));
	}

}
