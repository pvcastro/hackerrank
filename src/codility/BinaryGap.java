package codility;

public class BinaryGap {
    
    public static void main(String[] args) {
        int integer = 328;
        System.out.println(Integer.toBinaryString(integer));
        System.out.println(new BinaryGap().solution(integer));
    }

    public int solution(int N) {
        String binary = Integer.toBinaryString(N);
        int count = 0;
        int largestCount = 0;
        for (Character character : binary.toCharArray()) {
            if (character == '1') {
                if (count > largestCount) {
                    largestCount = count;
                }
                count = 0;
            } else if (character == '0') {
                count++;
            }
        }
        return largestCount;
    }

}
