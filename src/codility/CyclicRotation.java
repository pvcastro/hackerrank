package codility;

public class CyclicRotation {
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public int[] solution(int[] A, int K) {
        int[] result = new int[A.length];

        int length = A.length;
        int lastIndex = A.length - 1;
        
        int actualShift = 0;

        if (length > 0) {
            actualShift = K % length;
        }

        for (int i = 0; i < A.length; i++) {
            int newIndex;
            if (i + actualShift > lastIndex) {
                newIndex = i + actualShift - length;
            } else {
                newIndex = i + actualShift;
            }
            result[newIndex] = A[i];
        }

        return result;
    }

}
